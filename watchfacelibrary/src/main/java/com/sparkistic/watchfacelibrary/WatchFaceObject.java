/*
 * Copyright (C) 2015 Sparkistic, LLC
 *
 * Licensed under under a Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International License.
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sparkistic.watchfacelibrary;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.format.Time;
import android.view.SurfaceHolder;

/**
 * Be sure to have a constructor that takes the WatchFaceCallbacks a parameter
 */
public interface WatchFaceObject {

    /**
     * Called when the view is initialized
     * @param holder - handle to the watch face surface
     */
    public void onCreate(SurfaceHolder holder);

    /**
     * Called when the view is destroyed
     */
    public void onDestroy();

    /**
     * Called when burn in protection mode is changed
     * @param isBurnInProtectionOn - true if activated
     */
    public void onBurnInProtectionChanged(boolean isBurnInProtectionOn);

    /**
     * Called when low bit ambient mode is changed
     * @param isLowBitAmbientOn - true if activated
     */
    public void onLowBitAmbientChanged(boolean isLowBitAmbientOn);

    /**
     * Called when timer ticks to next second
     */
    public void onTimeTick();

    /**
     * Called when ambient (low power) mode is changed
     * @param inAmbientMode - true if activated
     */
    public void onAmbientModeChanged(boolean inAmbientMode);

    /**
     * Called when the watchface should draw itself
     * @param canvas - handle to the canvas object
     * @param bounds - size of the canvas
     * @param time - current time
     */
    public void onDraw(Canvas canvas, Rect bounds, Time time);

    /**
     * Called when mute mode is changed
     * @param inMuteMode - true if activated
     */
    public void onMuteModeChanged(boolean inMuteMode);

    /**
     * Called when visibility of the view is changed
     * @param visible - true if visible
     */
    public void onVisibilityChanged(boolean visible);
}
