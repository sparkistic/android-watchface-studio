/*
 * Copyright (C) 2015 Sparkistic, LLC
 *
 * Licensed under under a Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International License.
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sparkistic.watchfacelibrary;

import android.app.Service;
import android.content.Context;

public interface WatchFaceCallbacks {

    /**
     * Use invalidateView to redraw the view when necessary
     */
    public void invalidateView();

    /**
     * Use getAppContext to retrieve application context for
     * access to drawables and other resources
     * @return the application context
     */
    public Context getAppContext();
}
