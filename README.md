# Android WatchFace Studio #

Android WatchFace Studio provides an easy way for you to see your watch face in the emulator and quickly switch options, such as circle mode, chin on/off, and ambient mode. Using a shared library, you can use the same code to compile into your deployable watch face.

- - - -

# Getting Started #

There are two separate projects you will load separately into Android Studio. Both of these projects use the same library, **watchfacelibrary**. You should not modify the two interface files, WatchFaceCallbacks and WatchFaceObject. Instead, implement the methods in WatchFaceObject in your library project. The most important of these is onDraw. The analog watch face from the sample code is input only as an example.

## WatchfaceStudio ##
This app lets you see the watch face and easily view how it will look on rectangular and circular watches of different sizes. Easily toggle between ambient and other modes. This project should be run in the emulator.
To import this project into Android Studio, simply click File -> Import on the build.gradle file as indicated in the screenshot below:

![Import Screenshot](https://bitbucket.org/sparkistic/android-watchface-studio/raw/master/import-screenshot.png)

## ExampleWatchface ##
This is the project you will deploy to the Play store after you have completed the design for your watch face. Note that you will have to put in your developer key in **both** of the build.gradle files (mobile and wear). Stubbed out code is in there to get you started.

- - - -

# Contribution guidelines #

* Pull requests gladly accepted. We'd love to have help to improve this project!