/*
 * Copyright (C) 2015 Sparkistic, LLC
 *
 * Licensed under under a Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International License.
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sparkistic.watchfacestudio;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    private int horizontalSize = 320;
    private int verticalSize = 320;
    private int chinSize = 30;
    private static final int enabledTextColor = 0xFF555555;
    private static final int disabledTextColor = 0xFF888888;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView chinModeText = (TextView) findViewById(R.id.chinModeText);
        LinearLayout chinModeLayout = (LinearLayout) findViewById(R.id.chinModeLayout);
        chinModeLayout.setClickable(false);
        chinModeText.setTextColor(disabledTextColor);
    }

    public void onAmbientModeCheckBoxClicked(View view) {
        WatchFaceView wfv = (WatchFaceView) findViewById(R.id.watchface);
        CheckBox cb = (CheckBox) findViewById(R.id.ambientModeCheckBox);
        wfv.setAmbientMode(cb.isChecked());
    }

    public void onLowBitModeCheckBoxClicked(View view) {
        WatchFaceView wfv = (WatchFaceView) findViewById(R.id.watchface);
        CheckBox cb = (CheckBox) findViewById(R.id.lowBitModeCheckBox);
        wfv.setLowBitMode(cb.isChecked());
    }

    public void onBurnInProtectionCheckBoxClicked(View view) {
        WatchFaceView wfv = (WatchFaceView) findViewById(R.id.watchface);
        CheckBox cb = (CheckBox) findViewById(R.id.burnInProtectionCheckBox);
        wfv.setLowBitMode(cb.isChecked());
    }

    public void onCircleModeCheckBoxClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.circleModeCheckBox);
        CheckBox chinModeCb = (CheckBox) findViewById(R.id.chinModeCheckBox);
        LinearLayout chinModeLayout = (LinearLayout) findViewById(R.id.chinModeLayout);
        TextView chinModeText = (TextView) findViewById(R.id.chinModeText);

        if (cb.isChecked()) {
            chinModeCb.setEnabled(true);
            chinModeLayout.setClickable(true);
            chinModeText.setTextColor(enabledTextColor);
            cutCircleFromASquare(chinModeCb.isChecked());
        } else {
            chinModeCb.setEnabled(false);
            chinModeLayout.setClickable(false);
            chinModeText.setTextColor(disabledTextColor);
            resetClippingImageView();
        }
    }

    public void onChinModeCheckBoxClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.chinModeCheckBox);
        if (cb.isChecked()) {
            cutCircleFromASquare(true);
        } else {
            cutCircleFromASquare(false);
        }
    }

    public void onDimensionsChanged() {
        TextView horiz = (TextView) findViewById(R.id.horizontalSizeText);
        TextView vert = (TextView) findViewById(R.id.verticalSizeText);
        TextView chin = (TextView) findViewById(R.id.chinSizeText);
        WatchFaceView wfv = (WatchFaceView) findViewById(R.id.watchface);
        ImageView image = (ImageView) findViewById(R.id.clippingCircle);
        LinearLayout topLayout = (LinearLayout) findViewById(R.id.topLayout);

        horiz.setText(String.valueOf(horizontalSize));
        vert.setText(String.valueOf(verticalSize));
        chin.setText(String.valueOf(chinSize));

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) wfv.getLayoutParams();
        layoutParams.height = verticalSize;
        layoutParams.width = horizontalSize;

        wfv.setLayoutParams(layoutParams);
        image.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParamsTopLayout = (RelativeLayout.LayoutParams) topLayout.getLayoutParams();
        layoutParamsTopLayout.height = verticalSize + 50;
        topLayout.setLayoutParams(layoutParamsTopLayout);

        onCircleModeCheckBoxClicked(null);
    }

    public void onDimensionChangeButtonClick(View view) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.size_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInputHorizontal = (EditText) promptsView
                .findViewById(R.id.editTextDialogHorizontal);
        userInputHorizontal.setText(String.valueOf(horizontalSize));

        final EditText userInputVertical = (EditText) promptsView
                .findViewById(R.id.editTextDialogVertical);
        userInputVertical.setText(String.valueOf(verticalSize));

        final EditText userInputChin = (EditText) promptsView
                .findViewById(R.id.editTextDialogChin);
        userInputChin.setText(String.valueOf(chinSize));

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                horizontalSize = Integer.valueOf(userInputHorizontal.getText().toString());
                                verticalSize = Integer.valueOf(userInputVertical.getText().toString());
                                chinSize = Integer.valueOf(userInputChin.getText().toString());
                                onDimensionsChanged();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void cutCircleFromASquare(boolean isChinOn) {
        ImageView image = (ImageView) findViewById(R.id.clippingCircle);
        int width = image.getWidth();
        int height = image.getHeight();

        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        int color = 0xff00ff00;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);

        paint.setAntiAlias(true);
        canvas.drawARGB(255, 170, 170, 170);
        paint.setColor(color);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawCircle(width / 2, height / 2, width / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        if (isChinOn) {
            paint.setXfermode(null);
            color = 0xffaaaaaa;
            paint.setColor(color);
            canvas.drawRect(0, height - chinSize, width, height, paint);
        }
        canvas.drawBitmap(output, rect, rect, paint);
        image.setImageBitmap(output);
    }

    private void resetClippingImageView() {
        ImageView image = (ImageView) findViewById(R.id.clippingCircle);
        int width = image.getWidth();
        int height = image.getHeight();
        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        canvas.drawARGB(0, 0, 0, 0);
        image.setImageBitmap(output);
    }

    public void onCircleModeLayoutClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.circleModeCheckBox);
        cb.setChecked(!cb.isChecked());
        onCircleModeCheckBoxClicked(cb);
    }

    public void onChinModeLayoutClicked(View view) {
        CheckBox cbChin = (CheckBox) findViewById(R.id.chinModeCheckBox);
        cbChin.setChecked(!cbChin.isChecked());
        onChinModeCheckBoxClicked(cbChin);
    }

    public void onBurnInProtectionLayoutClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.burnInProtectionCheckBox);
        cb.setChecked(!cb.isChecked());
        onBurnInProtectionCheckBoxClicked(cb);
    }

    public void onAmbientModeLayoutClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.ambientModeCheckBox);
        cb.setChecked(!cb.isChecked());
        onAmbientModeCheckBoxClicked(cb);
    }

    public void onLowBitModeLayoutClicked(View view) {
        CheckBox cb = (CheckBox) findViewById(R.id.lowBitModeCheckBox);
        cb.setChecked(!cb.isChecked());
        onLowBitModeCheckBoxClicked(cb);
    }
}
