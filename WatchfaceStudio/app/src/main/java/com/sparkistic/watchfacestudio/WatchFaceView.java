/*
 * Copyright (C) 2015 Sparkistic, LLC
 *
 * Licensed under under a Creative Commons Attribution-NonCommercial-ShareAlike
 * 4.0 International License.
 *
 *      http://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sparkistic.watchfacestudio;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.View;

import com.sparkistic.watchfacelibrary.AnalogWatchFace;
import com.sparkistic.watchfacelibrary.WatchFaceCallbacks;
import com.sparkistic.watchfacelibrary.WatchFaceObject;

import java.util.concurrent.atomic.AtomicBoolean;


public class WatchFaceView extends View implements WatchFaceCallbacks {

    private WatchFaceObject watchFaceObject;
    private Time time;
    private Handler myHandler;
    private AtomicBoolean isReady = new AtomicBoolean(false);

    public WatchFaceView(Context context) {
        super(context);
        init();
    }

    public WatchFaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WatchFaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        watchFaceObject = new AnalogWatchFace(this);
        myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run(){
                checkForNonNullContext();
            }
        }, 250);
        time = new Time();
    }

    private void checkForNonNullContext() {
        //occasionally emulator does not have context right away
        //here we make sure we have context before drawing watch face
        if (isReady.get()) return;
        if (getAppContext() == null) {
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run(){
                    checkForNonNullContext();
                }
            }, 750);
        } else {
            isReady.set(true);
            myHandler.removeCallbacksAndMessages(null);
            watchFaceObject.onCreate(null);
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!isReady.get()) return;
        super.onDraw(canvas);
        int[] ints = new int[2];
        this.getLocationOnScreen(ints);
        Rect rect = new Rect(ints[0], ints[1], ints[0] + this.getWidth(), ints[1] + this.getHeight());
        watchFaceObject.onDraw(canvas, rect, time);
    }

    @Override
    public void invalidateView() {
        if (!isReady.get()) return;
        invalidate();
    }

    @Override
    public Context getAppContext() {
        return MyApplication.getAppContext();
    }

    public void setAmbientMode(boolean isAmbientModeOn) {
        watchFaceObject.onAmbientModeChanged(isAmbientModeOn);
    }

    public void setLowBitMode(boolean isLowBitModeOn) {
        watchFaceObject.onLowBitAmbientChanged(isLowBitModeOn);
    }
}
